package assignment.daft.com.daftassignment.app.CustomViews;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;

import com.android.volley.toolbox.NetworkImageView;

/**
 * Created by Derek on 03/12/2014.
 * Extension of Volley's NetworkImageView. View fades into visibility when set with an image.
 */
public class AnimatedNetworkImageView extends NetworkImageView {

    public AnimatedNetworkImageView(Context context) {

        super(context);
    }

    public AnimatedNetworkImageView(Context context, AttributeSet attributes) {

        super(context, attributes);
    }

    public AnimatedNetworkImageView(Context context, AttributeSet attributes, int defStyle) {

        super(context, attributes, defStyle);
    }

    @Override
    public void setImageBitmap(Bitmap bitmap) {

        boolean alreadyLoaded = getDrawable() != null;

        super.setImageBitmap(bitmap);

        // Only fade in view if setting image for the first time
        if(!alreadyLoaded) {

            AlphaAnimation fadeInAnimation = new AlphaAnimation(0, 1);
            fadeInAnimation.setDuration(500);
            fadeInAnimation.setInterpolator(new DecelerateInterpolator());
            startAnimation(fadeInAnimation);
        }
    }
}

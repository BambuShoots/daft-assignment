package assignment.daft.com.daftassignment.app.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import assignment.daft.com.daftassignment.R;

/**
 * Created by Derek on 03/12/2014.
 */
public class Utility {

    /**
     * Checks to see whether the device is currently connected to an internet network
     * @param context
     * @return True if currently connected, false if disconnected
     */
    public static boolean connectedToInternet(Context context) {

        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        if(networkInfo == null || !networkInfo.isConnected()) {

            new AlertDialog.Builder(context).setTitle(R.string.dialog_title_no_network)
                    .setIcon(context.getResources().getDrawable(android.R.drawable.ic_dialog_alert))
                    .setMessage(R.string.dialog_message_no_service)
                    .setNeutralButton(R.string.button_ok, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            // Dismiss
                        }
                    }).create().show();

            return false;

        } else {

            return true;
        }
    }
}

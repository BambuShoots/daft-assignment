package assignment.daft.com.daftassignment.app.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Derek on 03/12/2014.
 */
public class House implements Parcelable {

    private String imageURL;
    private String address;

    public House(String imageURL, String address) {

        this.imageURL = imageURL;
        this.address = address;
    }

    public String getAddress() {

        return address;
    }

    public String getImageURL() {

        return imageURL;
    }

    // ----------------------- Parcelable Implementation --------------------------

    private House(Parcel parcel) {

        this.imageURL = parcel.readString();
        this.address = parcel.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {

        parcel.writeString(imageURL);
        parcel.writeString(address);
    }

    @Override
    public int describeContents() {

        return 0;
    }

    public static final Creator<House> CREATOR = new Creator<House>() {

        @Override
        public House createFromParcel(Parcel parcel) {

            return new House(parcel);
        }

        @Override
        public House[] newArray(int size) {

            return new House[size];
        }
    };
}

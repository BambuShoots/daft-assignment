package assignment.daft.com.daftassignment.app.Fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import assignment.daft.com.daftassignment.R;
import assignment.daft.com.daftassignment.app.Activities.ActivityOpeningScreen;
import assignment.daft.com.daftassignment.app.Models.House;
import assignment.daft.com.daftassignment.app.Network.VolleyLoader;
import assignment.daft.com.daftassignment.app.Utils.Constants;
import assignment.daft.com.daftassignment.app.Utils.Utility;

/**
 * Created by Derek on 03/12/2014.
 */
public class FragmentOpeningScreen extends Fragment implements View.OnClickListener {

    private Button goButton;
    private List<House> houses;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        houses = new ArrayList<House>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_opening_screen, container, false);

        goButton = (Button) rootView.findViewById(R.id.go_button);
        goButton.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View view) {

        if(view == goButton) {

            // Check network connection before attempting to fetch data
            if(Utility.connectedToInternet(getActivity())) {

                // Show loading dialog whilst fetching data
                final ProgressDialog progressDialog = new ProgressDialog(getActivity());
                progressDialog.setTitle(getString(R.string.dialog_title_fetching_data));
                progressDialog.setMessage(getString(R.string.dialog_message_please_wait));
                progressDialog.setCancelable(false);
                progressDialog.setIndeterminate(true);
                progressDialog.show();

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, constructQuery(), null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            // Clear previous search results
                            if(houses.size() > 0) {

                                houses.clear();
                            }

                            JSONObject result = response.getJSONObject(Constants.JSON_RESULT);
                            JSONObject results = result.getJSONObject(Constants.JSON_RESULTS);
                            JSONArray ads = results.getJSONArray(Constants.JSON_ADS);

                            for(int i = 0; i < ads.length(); i++) {

                                JSONObject ad = ads.getJSONObject(i);
                                houses.add(new House(ad.getString(Constants.JSON_SMALL_THUMBNAIL_URL), ad.getString(Constants.JSON_FULL_ADDRESS)));
                            }

                            // Send results to next fragment
                            Bundle bundle = new Bundle();
                            bundle.putParcelableArrayList(Constants.HOUSES, (ArrayList<House>) houses);
                            FragmentListing fragmentListing = new FragmentListing();
                            fragmentListing.setArguments(bundle);

                            progressDialog.dismiss();
                            ((ActivityOpeningScreen) getActivity()).changeFragment(fragmentListing);

                        } catch (JSONException exc) {

                            progressDialog.dismiss();

                            final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                            alertDialog.setTitle(getString(R.string.dialog_title_fetching_failed));
                            alertDialog.setMessage(getString(R.string.dialog_message_contact_daft));
                            alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                            alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.button_ok), new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    alertDialog.dismiss();
                                }
                            });
                            alertDialog.show();
                        }
                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        progressDialog.dismiss();

                        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                        alertDialog.setTitle(getString(R.string.dialog_title_fetching_failed));
                        alertDialog.setMessage(getString(R.string.dialog_message_try_again));
                        alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.button_ok), new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                alertDialog.dismiss();
                            }
                        });
                        alertDialog.show();
                    }
                });

                jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10 * 1000, 1, 1.0f));

                VolleyLoader.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
            }
        }
    }

    /**
     * Construct the search query to Daft API
     * @return
     */
    private String constructQuery() {

        StringBuilder query = new StringBuilder();

        query.append(Constants.DAFT_BASE_URL).append(Constants.DAFT_SEARCH_SALE_URL).append(Constants.DAFT_ADD_PARAMETERS_URL).append(Constants.DAFT_API_KEY_PARAMETER)
                .append("\"" + getString(R.string.daft_api_key) + "\"").append(",").append(Constants.DAFT_PERPAGE_PARAMETER).append(Constants.DAFT_END_PARAMETERS_URL);

        return query.toString();
    }
}

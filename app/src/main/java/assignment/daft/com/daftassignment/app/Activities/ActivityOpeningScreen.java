package assignment.daft.com.daftassignment.app.Activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import java.util.Stack;

import assignment.daft.com.daftassignment.R;
import assignment.daft.com.daftassignment.app.Fragments.FragmentOpeningScreen;

/**
 * Created by Derek on 03/12/2014.
 */
public class ActivityOpeningScreen extends ActionBarActivity {

    // Stack to keep track of fragments in the app
    private Stack<Fragment> fragmentStack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        fragmentStack = new Stack<Fragment>();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Sets up custom toolbar as action bar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        changeFragment(new FragmentOpeningScreen());
    }

    @Override
    public void onBackPressed() {

        if(fragmentStack.size() > 1) {

            // Remove current fragment and switch to previous one
            fragmentStack.pop();
            changeFragmentNoFragmentStack(fragmentStack.peek());

        } else {

            // Exits out of app if there is no previous fragment
            super.onBackPressed();
        }
    }

    /**
     * Switches the current fragment shown in the activity
     * @param fragment The new fragment to show
     */
    public void changeFragment(Fragment fragment) {

        // If we already have fragment in the stack, remove old one and add new one
        for(int i = 0; i < fragmentStack.size(); i++) {

            if(((Object) fragment).getClass().equals(((Object) fragmentStack.get(i)).getClass())) {

                fragmentStack.remove(fragmentStack.get(i));
                break;
            }
        }

        fragmentStack.add(fragment);

        getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).replace(R.id.fragment_container, fragment).commit();
    }

    /**
     * Switches current fragment without adding the new fragment to the fragmentStack
     * @param fragment The new fragment to show
     */
    public void changeFragmentNoFragmentStack(Fragment fragment) {

        getSupportFragmentManager().beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).replace(R.id.fragment_container, fragment).commit();
    }
}

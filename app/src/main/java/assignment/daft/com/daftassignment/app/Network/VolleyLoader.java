package assignment.daft.com.daftassignment.app.Network;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by Derek on 03/12/2014.
 */
public class VolleyLoader {

    private static VolleyLoader volleyLoader;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;
    private static Context context;

    private VolleyLoader(Context context) {

        this.context = context;
        requestQueue = getRequestQueue();

        imageLoader = new ImageLoader(requestQueue, new LruBitmapCache(LruBitmapCache.getCacheSize(context)));
    }

    public static synchronized VolleyLoader getInstance(Context context) {

        if(volleyLoader == null) {

            volleyLoader = new VolleyLoader(context);
        }

        return volleyLoader;
    }

    public RequestQueue getRequestQueue() {

        if(requestQueue == null) {

            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> request) {

        getRequestQueue().add(request);
    }

    public ImageLoader getImageLoader() {

        return imageLoader;
    }
}

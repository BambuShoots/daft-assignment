package assignment.daft.com.daftassignment.app.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import assignment.daft.com.daftassignment.R;
import assignment.daft.com.daftassignment.app.Activities.ActivityOpeningScreen;
import assignment.daft.com.daftassignment.app.Adapter.CardsAdapter;
import assignment.daft.com.daftassignment.app.Models.House;
import assignment.daft.com.daftassignment.app.Utils.Constants;

/**
 * Created by Derek on 03/12/2014.
 */
public class FragmentListing extends Fragment {

    private RecyclerView houseList;
    private CardsAdapter adapter;

    private String layoutStyle;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();

        layoutStyle = bundle.getString(Constants.LAYOUT_STYLE);

        // Set default style if none set
        if(layoutStyle == null) {

            layoutStyle = Constants.LIST_STYLE;
        }

        List<House> houses = bundle.getParcelableArrayList(Constants.HOUSES);
        adapter = new CardsAdapter(getActivity(), houses, layoutStyle);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_listing, container, false);

        houseList = (RecyclerView) rootView.findViewById(R.id.house_list);
        houseList.setAdapter(adapter);

        if(layoutStyle.equals(Constants.GRID_STYLE)) {

            GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2, LinearLayoutManager.VERTICAL, false);
            houseList.setLayoutManager(layoutManager);

        } else {

            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            houseList.setLayoutManager(layoutManager);
        }

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        if(layoutStyle.equals(Constants.GRID_STYLE)) {

            inflater.inflate(R.menu.menu_list, menu);

        } else {

            inflater.inflate(R.menu.menu_grid, menu);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        switch(menuItem.getItemId()) {

            case R.id.action_list:
            case R.id.action_grid:

                Bundle bundle = getArguments();
                FragmentListing fragmentListing = new FragmentListing();

                if(layoutStyle.equals(Constants.GRID_STYLE)) {

                    bundle.putString(Constants.LAYOUT_STYLE, Constants.LIST_STYLE);

                } else {

                    bundle.putString(Constants.LAYOUT_STYLE, Constants.GRID_STYLE);
                }

                fragmentListing.setArguments(bundle);
                ((ActivityOpeningScreen) getActivity()).changeFragment(fragmentListing);

                return true;
        }

        return super.onOptionsItemSelected(menuItem);
    }
}

package assignment.daft.com.daftassignment.app.Utils;

/**
 * Created by Derek on 03/12/2014.
 */
public class Constants {

    // ------------------------------- Data -------------------------------------------

    public static final String HOUSES = "Houses";

    // ------------------------------ Network -----------------------------------------

    public static final String DAFT_BASE_URL = "https://api.daft.com/v2/json";
    public static final String DAFT_SEARCH_SALE_URL = "/search_sale";
    public static final String DAFT_ADD_PARAMETERS_URL = "?parameters={";
    public static final String DAFT_END_PARAMETERS_URL = "}";
    public static final String DAFT_API_KEY_PARAMETER = "\"api_key\":";
    public static final String DAFT_PERPAGE_PARAMETER = "\"query\":{\"perpage\":50}";

    public static final String JSON_RESULT = "result";
    public static final String JSON_RESULTS = "results";
    public static final String JSON_ADS = "ads";
    public static final String JSON_SMALL_THUMBNAIL_URL = "small_thumbnail_url";
    public static final String JSON_FULL_ADDRESS = "full_address";

    // --------------------------- Layout Styles --------------------------------------

    public static final String LAYOUT_STYLE = "LayoutStyle";
    public static final String LIST_STYLE = "ListStyle";
    public static final String GRID_STYLE = "GridStyle";
}

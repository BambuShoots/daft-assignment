package assignment.daft.com.daftassignment.app.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;

import java.util.List;

import assignment.daft.com.daftassignment.R;
import assignment.daft.com.daftassignment.app.CustomViews.AnimatedNetworkImageView;
import assignment.daft.com.daftassignment.app.Models.House;
import assignment.daft.com.daftassignment.app.Network.VolleyLoader;
import assignment.daft.com.daftassignment.app.Utils.Constants;

/**
 * Created by Derek on 03/12/2014.
 */
public class CardsAdapter extends RecyclerView.Adapter<CardsAdapter.ViewHolder> {

    private List<House> houses;
    private ImageLoader imageLoader;
    private String layoutStyle;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView addressText;
        public AnimatedNetworkImageView imageView;

        public ViewHolder(View inflatedView) {

            super(inflatedView);
            addressText = (TextView) inflatedView.findViewById(R.id.address);
            imageView = (AnimatedNetworkImageView) inflatedView.findViewById(R.id.thumbnail);
            imageView.setErrorImageResId(R.drawable.icon_missing_image);
        }
    }

    public CardsAdapter(Context context, List<House> houses, String layoutStyle) {

        this.houses = houses;
        this.layoutStyle = layoutStyle;
        imageLoader = VolleyLoader.getInstance(context).getImageLoader();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup container, int viewType) {

        View inflatedView;

        // Check to see which layout style should be applied
        if(layoutStyle.equals(Constants.GRID_STYLE)) {

            inflatedView = LayoutInflater.from(container.getContext()).inflate(R.layout.cards_adapter_grid_item, container, false);

        } else {

            inflatedView = LayoutInflater.from(container.getContext()).inflate(R.layout.cards_adapter_list_item, container, false);
        }

        return new ViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {

        // Clear view state in case view has been recycled
        viewHolder.imageView.setImageBitmap(null);
        viewHolder.imageView.setImageUrl(houses.get(position).getImageURL(), imageLoader);

        viewHolder.addressText.setText(houses.get(position).getAddress());
    }

    @Override
    public int getItemCount() {

        return houses.size();
    }
}
